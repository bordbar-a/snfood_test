<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\User;
use App\Models\Vendor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $deliveryTime = rand(50, 120);
        return [
            'vendor_id' => Vendor::factory(),
            'user_id' => User::factory(),
            'status' => Order::STATUS_PREPARATION_AND_SEND,
            'delivery_time' => Carbon::now()->addMinute($deliveryTime),
        ];
    }
}
