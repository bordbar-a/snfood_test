<?php

namespace Database\Factories;

use App\Contracts\TripServiceInterface;
use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Trip>
 */
class TripFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $randomStatus = array_rand(resolve(TripServiceInterface::class)->allStatus());
        return [
            'order_id' => Order::factory(),
            'status' => $randomStatus
        ];
    }
}
