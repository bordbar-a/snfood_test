## snappfood challenge



### Getting started for test project

### In root directory :

##### if not exist .env file , you must create .env file from .env.example file.

1- docker-compose build

2- docker-compose up -d

3 - docker exec -it snappfood-app sh

4 - composer install

5 - php artisan optimize

6 - php artisan migrate

7 - php artisan db:seed

====================================================================

### Now you can test 3 api . 
base url by default is : http://localhost:8000

api 1 :

description : api for report order delay

api : {base_url}/api/web/customer/v1/delay-reports

method : post

body : order_id

====================================================================


api 2 :

description : api for get last 7 day delay report

api : {base_url}/api/web/admin/v1/delay-reports

method : get

====================================================================


api 3 :

description : api for assign an order in delayQueue to agent

api : {base_url}/api/web/admin/v1/delay-queue/pop

method : get
