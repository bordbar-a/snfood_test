<?php

use App\Http\Controllers\Admin\DelayQueueController;
use App\Http\Controllers\Admin\DelayReportController as DelayReportControllerAdmin;
use App\Http\Controllers\Customer\DelayReportController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('web/customer/v1/')
    ->name('web.customer.v1.')
    ->as('web.customer.v1.')
    ->group(function () {
        Route::prefix('delay-reports')
            ->name('delay-reports.')
            ->as('delay-reports.')
            ->group(function (): void {
                Route::post('/', [DelayReportController::class, 'register'])
                    ->name('register_delay');
            });
    });


Route::prefix('web/admin/v1/')
    ->name('web.admin.v1.')
    ->as('web.admin.v1.')
    ->group(function () {
        Route::prefix('delay-reports')
            ->name('delay-reports.')
            ->as('delay-reports.')
            ->group(function (): void {
                Route::get('/', [DelayReportControllerAdmin::class, 'all'])
                    ->name('all_delay');
            });

        Route::prefix('delay-queue')
            ->name('delay-queue.')
            ->as('delay-queue.')
            ->group(function (): void {
                Route::get('/pop', [DelayQueueController::class, 'pop'])
                    ->name('pop_delay');
            });
    });
