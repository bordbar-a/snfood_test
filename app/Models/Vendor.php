<?php

namespace App\Models;

use App\Models\Relations\VendorRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory, VendorRelations;


    protected $fillable = [
        'name'
    ];
}
