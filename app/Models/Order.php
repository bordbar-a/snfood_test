<?php

namespace App\Models;

use App\Models\Relations\OrderRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory, OrderRelations;

    const STATUS_PENDING = 0;
    const STATUS_PAID = 1;
    const STATUS_PREPARATION_AND_SEND = 2;
    const STATUS_DELAYED = 3;
    const STATUS_COMPLETED = 4;
    const STATUS_CANCELED = 5;

    protected $fillable = [
        'vendor_id',
        'user_id',
        'status',
        'delivery_time',
    ];
    protected $dates = ['delivery_time'];
}
