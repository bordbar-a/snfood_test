<?php

namespace App\Models;

use App\Models\Relations\DelayReportRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DelayReport extends Model
{
    use HasFactory,DelayReportRelations;

    protected $fillable =[
        'order_id',
        'vendor_id',
        'delay_in_minute',
    ];
}
