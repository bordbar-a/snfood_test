<?php

namespace App\Models\Relations;

use App\Models\Order;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait TripRelations
{
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
