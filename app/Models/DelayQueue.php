<?php

namespace App\Models;

use App\Models\Relations\DelayQueueRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DelayQueue extends Model
{
    use HasFactory, DelayQueueRelations;

    const STATUS_PENDING = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_DONE = 2;

    protected $fillable = [
        'user_id',
        'order_id',
        'status',
        'agent_description'
    ];
}
