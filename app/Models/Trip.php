<?php

namespace App\Models;

use App\Models\Relations\TripRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use HasFactory, TripRelations;

    const STATUS_AT_VENDOR = 0;
    const STATUS_ASSIGNED = 1;
    const STATUS_PICKED = 2;
    const STATUS_DELIVERED = 3;

    protected $fillable =[
        'order_id',
        'status',
    ];
}
