<?php

namespace App\Http\Controllers\Customer;

use App\Contracts\DelayReportServiceInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\DelayReportRegisterRequest;
use Illuminate\Http\JsonResponse;

class DelayReportController extends Controller
{

    public function __construct(private DelayReportServiceInterface $delayReportService)
    {
    }

    public function register(DelayReportRegisterRequest $registerRequest): JsonResponse
    {
        $result = $this->delayReportService->delayReportProcess($registerRequest->order_id);
        return response()->json([$result
        ], 200);
    }
}
