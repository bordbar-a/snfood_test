<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\DelayQueueServiceInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class DelayQueueController extends Controller
{

    public function __construct(private DelayQueueServiceInterface $delayQueueService)
    {
    }

    public function pop(): JsonResponse
    {
        $result = $this->delayQueueService->pop();
        return response()->json([
            'result' => $result
        ], 200);
    }
}
