<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\DelayReportServiceInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class DelayReportController extends Controller
{

    public function __construct(private DelayReportServiceInterface $delayReportService)
    {
    }

    public function all(): JsonResponse
    {
        $result = $this->delayReportService->all();
        return response()->json([
            'result'=>$result
        ], 200);
    }
}
