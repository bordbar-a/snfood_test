<?php

namespace App\Http\Requests;

use Helper\ResponseBuilder;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    abstract function rules(): array;

    protected function failedValidation(Validator $validator)
    {
        $response =resolve(ResponseBuilder::class)
            ->message(__('message.exception.data_invalid'))
            ->errors($validator->errors())
            ->statusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->json();
        throw new HttpResponseException($response);
    }
}
