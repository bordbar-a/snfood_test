<?php

namespace App\Http\Requests;

use App\Rules\DelayOrderRule;
use Illuminate\Foundation\Http\FormRequest;

class DelayReportRegisterRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'order_id' => ['required', 'exists:orders,id' , new DelayOrderRule],
        ];
    }
}
