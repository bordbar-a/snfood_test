<?php

namespace App\Rules;

use App\Contracts\OrderServiceInterface;
use Illuminate\Contracts\Validation\InvokableRule;

class DelayOrderRule implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if(!resolve(OrderServiceInterface::class)->isDelayOrder($value)){
            $fail('is not a delay order');
        };
    }
}
