<?php

namespace App\Services;

use App\Contracts\AgentServiceInterface;
use App\Models\DelayQueue;
use App\Models\User;

class AgentService implements AgentServiceInterface
{

    public function canGetOrderInDelayQueue(User $user): bool
    {
        $delayQueue = DelayQueue::where('user_id', $user->id)->whereIn('status', [DelayQueue::STATUS_PENDING, DelayQueue::STATUS_IN_PROGRESS])->get();
        $maxOrderCountInDelayQueue = config('delayqueue.max_order_count_in_delay_queue');
        return ($maxOrderCountInDelayQueue - count($delayQueue)) > 0;
    }
}
