<?php

namespace App\Services;

use App\Contracts\DelayReportServiceInterface;
use App\Contracts\OrderServiceInterface;
use App\Models\DelayReport;
use App\Models\Order;
use App\Services\DelayChecker\Checker;
use App\Services\DelayChecker\IsDelayOrderChecker;
use App\Services\DelayChecker\PutInQueueDelay;
use App\Services\DelayChecker\RecalculateDeliveryTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DelayReportService implements DelayReportServiceInterface
{

    public function __construct(private OrderServiceInterface $orderService)
    {
    }

    public function delayReportProcess(int $orderId)
    {
        $order = $this->orderService->getOrderWithTripById($orderId);
        return $this->checkerChain()->process($order);
    }

    private function checkerChain(): Checker
    {
        $putInQueueDelay = new  PutInQueueDelay();
        $recalculateDeliveryTime = new RecalculateDeliveryTime($putInQueueDelay);
        return new  IsDelayOrderChecker($recalculateDeliveryTime);
    }

    public function registerDelay(Order $order)
    {
        $delayTime = $this->orderService->calculateDelayTimeInMinute($order);
        return DelayReport::create(['order_id' => $order->id, 'vendor_id' => $order->vendor_id, 'delay_in_minute' => $delayTime]);
    }

    public function all()
    {
        $lastSevenDays = Carbon::now()->subDays(7);

        $stringTime = $lastSevenDays->toDateString();
        $query = "SELECT vendors.name , delay_table.total_delay
                  FROM vendors
                  JOIN (SELECT vendor_id ,SUM(delay_in_minute) AS 'total_delay' FROM `delay_reports` WHERE created_at > '" . $stringTime . "' GROUP BY vendor_id) delay_table
                  ON delay_table.vendor_id  = vendors.id ORDER BY delay_table.total_delay DESC";
        return DB::select(DB::raw($query));

    }
}
