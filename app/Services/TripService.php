<?php

namespace App\Services;

use App\Contracts\TripServiceInterface;
use App\Models\Trip;

class TripService implements TripServiceInterface
{

    public function allStatus(): array
    {
        return [Trip::STATUS_AT_VENDOR, Trip::STATUS_ASSIGNED, Trip::STATUS_PICKED, Trip::STATUS_DELIVERED];
    }


    public function tripStatusesForRecalculateDeliveryTime(): array
    {
        return [Trip::STATUS_AT_VENDOR, Trip::STATUS_ASSIGNED, Trip::STATUS_PICKED];
    }
}
