<?php

namespace App\Services;

use App\Contracts\OrderServiceInterface;
use App\Models\Order;
use Carbon\Carbon;
use Helper\EstimateTime;

class OrderService implements OrderServiceInterface
{

    public function getOrderWithTripById(int $orderId)
    {
        return Order::with('trip')->find($orderId);
    }

    public function isDelayOrder(int $orderId): bool
    {
        $order = Order::where(['id' => $orderId, 'status' => Order::STATUS_PREPARATION_AND_SEND])->first();
        if (is_null($order)) {
            return false;
        }
        $deliveryTime = $order->delivery_time;
        return $deliveryTime->lt(Carbon::now());
    }

    public function calculateEstimate(): Carbon
    {
        $deliveryInMinute = resolve(EstimateTime::class)->estimateSendingOrder();
        return Carbon::now()->addMinute($deliveryInMinute);
    }

    public function reCalculateOrderDeliveryTime(Order $order): Order
    {
        $newDeliveryTimeInMinute = $this->calculateEstimate();
        $order->delivery_time = $newDeliveryTimeInMinute;
        $order->save();
        return $order;

    }

    public function calculateDelayTimeInMinute(Order $order): int
    {

        if (!$this->isDelayOrder($order->id)) {
            return 0;
        }
        $deliveryTime = $order->delivery_time;
        return Carbon::now()->diffInMinutes($deliveryTime);
    }

    public function changeStatusTo(Order $order, int $status): Order
    {
        $order->status = $status;
        $order->save();
        return $order;
    }
}
