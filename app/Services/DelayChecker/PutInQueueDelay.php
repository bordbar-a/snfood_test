<?php

namespace App\Services\DelayChecker;

use App\Contracts\DelayQueueServiceInterface;
use App\Contracts\DelayReportServiceInterface;
use App\Contracts\OrderServiceInterface;
use App\Contracts\TripServiceInterface;
use App\Models\Order;

class PutInQueueDelay extends Checker
{

    public function process(Order $order)
    {
        $tripService = resolve(TripServiceInterface::class);
        $trip = $order->trip;

        $orderService = resolve(OrderServiceInterface::class);
        $delayReportService = resolve(DelayReportServiceInterface::class);
        $delayQueueService = resolve(DelayQueueServiceInterface::class);

        if (is_null($trip) || !in_array($trip->status, $tripService->tripStatusesForRecalculateDeliveryTime())) {
            if ($delayQueueService->inDelayQueueWithoutCompletedStatus($order)) {
                return ['message' => 'already exist in queue for check by agent'];
            }
            $delayReportService->registerDelay($order);
            $delayQueueService->register($order);
            $orderService->changeStatusTo($order, Order::STATUS_DELAYED);
            return ['message' => 'put in delay queue for check by agent'];
        }
        return parent::process($order);
    }
}
