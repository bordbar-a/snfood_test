<?php

namespace App\Services\DelayChecker;

use App\Models\Order;

abstract class Checker
{
    private $nextChecker;

    /**
     * @param $nextChecker
     */
    public function __construct(Checker $nextChecker = null)
    {
        $this->nextChecker = $nextChecker;
    }


    public function process(Order $order)
    {
        if ($this->nextChecker) {
            return $this->nextChecker->process($order);
        }
        return ['result' => 'nothing action'];
    }


}
