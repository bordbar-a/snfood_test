<?php

namespace App\Services\DelayChecker;

use App\Contracts\DelayReportServiceInterface;
use App\Contracts\OrderServiceInterface;
use App\Contracts\TripServiceInterface;
use App\Models\Order;

class RecalculateDeliveryTime extends Checker
{

    public function process(Order $order)
    {

        $tripService = resolve(TripServiceInterface::class);
        $trip = $order->trip;

        $orderService = resolve(OrderServiceInterface::class);
        $delayReportService = resolve(DelayReportServiceInterface::class);

        if (!is_null($trip) && in_array($trip->status, $tripService->tripStatusesForRecalculateDeliveryTime())) {
            $delayReportService->registerDelay($order);
            $order = resolve(OrderServiceInterface::class)->reCalculateOrderDeliveryTime($order);
            return ['new_delivery_time' => $order->delivery_time];
        }
        return parent::process($order);
    }
}
