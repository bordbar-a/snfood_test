<?php

namespace App\Services;

use App\Contracts\AgentServiceInterface;
use App\Contracts\DelayQueueServiceInterface;
use App\Models\DelayQueue;
use App\Models\Order;
use App\Models\User;

class DelayQueueService implements DelayQueueServiceInterface
{
    public function __construct(private AgentServiceInterface $agentService)
    {
    }

    public function register(Order $order): DelayQueue
    {
        $data = ['order_id' => $order->id, 'vendor_id' => $order->vendor_id, 'status' => DelayQueue::STATUS_PENDING];
        return DelayQueue::create($data);
    }

    public function inDelayQueueWithoutCompletedStatus(Order $order): bool
    {
        $delayQueue = DelayQueue::where('order_id', $order->id)->whereIn('status', [DelayQueue::STATUS_PENDING, DelayQueue::STATUS_IN_PROGRESS])->first();
        return !is_null($delayQueue);
    }

    public function pop(): array
    {
        //we must get login user , but here for test , we get random user_id
        $user = User::inRandomOrder()->first();

        if (!$this->agentService->canGetOrderInDelayQueue($user)) {
            return ['message' => 'you have a delay order to check'];
        }

        $delayQueue = DelayQueue::where('status', DelayQueue::STATUS_PENDING)->where('user_id', null)->orderBy('created_at', 'asc')->first();

        if (is_null($delayQueue)) {
            return ['message' => 'delay queue is empty'];
        }

        $delayQueue->user_id = $user->id;
        $delayQueue->save();
        return ['delay_id' => $delayQueue->id];

    }
}
