<?php

namespace App\Providers;

use App\Contracts\AgentServiceInterface;
use App\Contracts\DelayQueueServiceInterface;
use App\Contracts\DelayReportServiceInterface;
use App\Contracts\OrderServiceInterface;
use App\Contracts\TripServiceInterface;
use App\Contracts\UserServiceInterface;
use App\Contracts\VendorServiceInterface;
use App\Services\AgentService;
use App\Services\DelayQueueService;
use App\Services\DelayReportService;
use App\Services\OrderService;
use App\Services\TripService;
use App\Services\UserService;
use App\Services\VendorService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindContractsToConcrete();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function bindContractsToConcrete(): void
    {
        $this->app->bind(DelayReportServiceInterface::class, DelayReportService::class);
        $this->app->bind(OrderServiceInterface::class, OrderService::class);
        $this->app->bind(TripServiceInterface::class, TripService::class);
        $this->app->bind(UserServiceInterface::class, UserService::class);
        $this->app->bind(VendorServiceInterface::class, VendorService::class);
        $this->app->bind(DelayQueueServiceInterface::class, DelayQueueService::class);
        $this->app->bind(AgentServiceInterface::class, AgentService::class);
    }
}
