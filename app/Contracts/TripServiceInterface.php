<?php

namespace App\Contracts;

interface TripServiceInterface
{
    public function allStatus(): array;

    public function tripStatusesForRecalculateDeliveryTime(): array;
}
