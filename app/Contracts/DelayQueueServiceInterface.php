<?php

namespace App\Contracts;

use App\Models\DelayQueue;
use App\Models\Order;

interface DelayQueueServiceInterface
{
    public function register(Order $order): DelayQueue;

    public function inDelayQueueWithoutCompletedStatus(Order $order): bool;

    public function pop(): array;
}
