<?php

namespace App\Contracts;

use App\Models\Order;

interface DelayReportServiceInterface
{
    public function delayReportProcess(int $orderId);

    public function registerDelay(Order $order);

    public function all();
}
