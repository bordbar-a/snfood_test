<?php

namespace App\Contracts;

use App\Models\Order;

interface OrderServiceInterface
{
    public function getOrderWithTripById(int $orderId);

    public function isDelayOrder(int $orderId): bool;

    public function reCalculateOrderDeliveryTime(Order $order): Order;

    public function calculateEstimate();

    public function calculateDelayTimeInMinute(Order $order): int;

    public function changeStatusTo(Order $order , int $status):Order;
}
