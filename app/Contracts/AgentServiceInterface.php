<?php

namespace App\Contracts;

use App\Models\User;

interface AgentServiceInterface
{
    public function canGetOrderInDelayQueue(User $user): bool;
}
