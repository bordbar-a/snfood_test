<?php

namespace Helper;

class EstimateTime
{
    public function estimateSendingOrder(): int
    {

        $estimate = 40;
        try {
            $response = file_get_contents('https://run.mocky.io/v3/122c2796-5df4-461c-ab75-87c1192b17f7');
            $result = json_decode($response);
            if ($result->status === true) {
                $data = $result->data;
                $estimate = $data->eta;
            }
        } catch (\Exception $exception) {

        }
        return $estimate;
    }
}
