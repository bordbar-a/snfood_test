<?php

namespace Helper;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\MessageBag;

class ResponseBuilder
{
    private array $items = [];
    private MessageBag|null $errors = null;
    private int $statusCode = 200;

    public function message(string $message = ''): self
    {
        $this->message = $message;
        return $this;
    }

    public function errors(MessageBag $errors): self
    {

        $this->errors = $errors;
        return $this;
    }

    public function statusCode(int $statusCode = 200): self
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function items(array $items = []): self
    {
        $this->items = $items;
        return $this;
    }

    public function json(): JsonResponse
    {
        return response()->json(
            [
                'items' => $this->items,
                'errors' => $this->errors,
            ],
            $this->statusCode,
        );
    }
}
